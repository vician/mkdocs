# Inspiration: https://github.com/sequenceiq/docker-mkdocs/blob/master/Dockerfile
FROM ubuntu:latest
MAINTAINER SequenceIQ

RUN apt-get clean && apt-get update && apt-get install -y locales
RUN locale-gen en_US.UTF-8

ENV LANG en_US.UTF-8
ENV LC_ALL en_US.UTF-8

RUN apt-get -y upgrade
RUN apt-get install -y python3 python3-pip python3-dev build-essential libyaml-dev git rsync openssh-client tree

RUN pip3 install --upgrade pip
RUN pip3 install mkdocs mkdocs-material
