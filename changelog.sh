#!/bin/bash

MAX_COUNT_OF_CHANGED_FILES=5
count_of_files=0

index=1

grep HASH mkdocs.yml 1>/dev/null 2>/dev/null
if [ $? -eq 0 ]; then
	echo "Enabling dry run"
	DRYRUN=1
else
	DRYRUN=0
fi

alreadychanged=()
OLDIFS=$IFS
while true; do
	CI_COMMIT_SHA=$(git log --format="%H" -n $index | tail -n 1)
	files=($(git diff-tree --no-commit-id --name-only -r $CI_COMMIT_SHA))
	date="$(date -d @$(git log --format="%at" -n $index | tail -n 1) "+%Y-%m-%d")"
	first_item=1
	echo "- $date [${#files[@]}]:"
	for file in ${files[@]}; do
		echo -n "  - $file"
		if [ ! -f "$file" ]; then
			echo " --- not found!"
			continue
		fi
		if [[ "$file" =~ ^docs/ ]] && [[ "$file" =~ \.md$ ]] ; then
			echo $alreadychanged 2>/dev/null | grep $file 1>/dev/null 2>/dev/null
			if [ $? -eq 0 ]; then
				echo " ---> already written"
			   continue
			fi
			alreadychanged+=("$file")
			if [ $first_item -eq 1 ]; then
				if [ $DRYRUN -ne 1 ]; then
					echo "- [$date](https://gitlab.com/vician/wiki/commit/$CI_COMMIT_SHA):" >> docs/index.md
				fi
				first_item=0
			fi
			file=$(echo $file | sed 's/^docs\///g')
			file=$(echo $file | sed 's/\.md$//g')
			if [ $DRYRUN -ne 1 ]; then
				echo -e "\t- [$file]($file)" >> docs/index.md
			fi
			echo " ---> $file"
			let count_of_files++
		else
			echo " ---> skip"
		fi
	done
	if [ $index -eq 30 ]; then
		echo "-> too many commits"
		break
	fi
	if [ $count_of_files -ge $MAX_COUNT_OF_CHANGED_FILES ]; then
		echo "-> changed $count_of_files - exiting"
		break
	fi
	let index++
	echo "-> trying commit $index"
done
