# mkdocs
Skeleton for my mkdocs projects

## Installation

1. Create new repo for your project and go to its directory
2. Add this project as a submodule

		git submodule add git@gitlab.com:vician/mkdocs.git

3. Install

		cd mkdocs
		make install

## Update or init

1. Go to the project directory (not this project)
2. Update submodules

		git submodule init
		git submodule update
