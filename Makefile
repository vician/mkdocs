LIVEPID="live.pid"

LISTENIP="localhost"

bold=$$(tput bold)
normal=$$(tput sgr0)

EPIPHANY=epiphany --private-instance
BROWSER=$(EPIPHANY)


install:
	pip3 install --upgrade --user mkdocs mkdocs-material
	test -f ../Makefile || cp .Makefile.up ../Makefile
	test -f ../.gitignore || ln -s mkdocs/.gitignore ../.gitignore
	touch $(LIVEPID)
	mkdir -p ../docs/
	test -f ../mkdocs.yml || cp mkdocs.yml ../mkdocs.yml

BROWSER=epiphany

live:
	kill -0 $$(cat $(LIVEPID)) || (cd ..; mkdocs serve --dev-addr=$(LISTENIP):0 1>/dev/null 2>/dev/null & echo $$! > $(LIVEPID))
	sleep 1
	$(BROWSER) "http://$(LISTENIP):$$(lsof -Pan -p $$(cat $(LIVEPID)) -i | grep LISTEN | awk '{print $$9}' | awk -F':' '{print $$2}')/" & 1>/dev/null 2>/dev/null 3>/dev/null

check:
	kill -0 $$(cat $(LIVEPID)) || echo "${bold}live is not running${normal}"

stop:
	kill -0 $$(cat $(LIVEPID)) && kill $$(cat $(LIVEPID)) || echo "not running"

clean:
	cd .. && mkdocs build --clean

build:
	cd .. && mkdocs build

restart: stop clean live
